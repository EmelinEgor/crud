package com.crud.api;

import com.crud.dao.BuyerDao;


import com.crud.model.Buyer;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
public class RestApi {
    private BuyerDao buyerDao;

    public void setBuyerDao(BuyerDao buyerDao) {
        this.buyerDao = buyerDao;
    }

    @GetMapping(value = "/get", produces = MediaType.APPLICATION_JSON_VALUE)
    public List getAll() {
        return buyerDao.getAll();
    }

    @GetMapping(value = "/get/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Buyer getById(@PathVariable("id") int id) {
        return buyerDao.getById(id);
    }

    @PostMapping(value = "/create", produces = MediaType.APPLICATION_JSON_VALUE)
    public String create(@RequestBody Buyer buyer) {
        return buyerDao.create(buyer);
    }

    @DeleteMapping(value = "/delete", produces = MediaType.APPLICATION_JSON_VALUE)
    public String deleteAll() {
        return buyerDao.deleteAll();
    }

    @DeleteMapping(value = "/delete/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public String deleteById(@PathVariable("id") int id) {
        return buyerDao.deleteById(id);
    }


}
