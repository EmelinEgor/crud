package com.crud.dao;

import com.crud.model.Buyer;

import com.crud.model.BuyerMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;


@Repository
public class BuyerDao {

    private DataSource dataSource;
    private JdbcTemplate jdbcTemplate;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }


    public List getAll(){
        String sql = "SELECT * FROM buyer";
        List buyers = jdbcTemplate.query(sql, new BuyerMapper());
        return buyers;
    }

    public Buyer getById(int id){
        String SQL = "SELECT * FROM buyer WHERE id = ?";
        return jdbcTemplate.queryForObject(SQL, new Object[]{id}, new BuyerMapper());
    }

    public String create(Buyer buyer){
        String SQL = "insert into buyer VALUES (?,?,?,?)";
        jdbcTemplate.update(SQL, buyer.getId(), buyer.getName(), buyer.getDistrict(),buyer.getDiscount());
        return "Buyer was successfully created";
    }

    public String deleteAll (){
        String sql = "delete from buyer";
        jdbcTemplate.update(sql);
        return "Buyers was successfully deleted";
    }

    public String deleteById(int id){
        String sql = "delete from buyer where id = ?";
        jdbcTemplate.update(sql, id);
        return "Buyer was successfully deleted";
    }
}
